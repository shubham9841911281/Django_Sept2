from django.urls import path
from shubhamapp.views import homepage, category

urlpatterns = [
    path('', homepage, name='Homepage'),
    path('category/<int:category_id>', category, name='category_page')
]
