from django.shortcuts import render
from django.http import HttpResponse
from .models import Category


# Create your views here.

def hello(request):
    # print(type(request))
    # return HttpResponse

    context = {
        'numbers': range(50),
        'test': False,

    }
    return render(request, 'NEW_INDEX.html', context)

def homepage(request):
    context= {
        'categories' : Category.objects.all()
    }
    return render(request, 'blog.html',context)

def category(request, category_id):
    cat= Category.objects.get(pk=category_id)
    context = {
        'categories': Category.objects.all(),
        'cat' : cat,
        'blogs':cat.blog_set.all()
    }
    return render(request, 'blog_1.html',context)


